export const places = [
	{ label: 'Centro de Tecnologia', value: "Centro de Tecnologia" },
	{ label: 'Faculdade Nacional de Direito', value: "Faculdade Nacional de Direito" },
	{ label: 'Praia Vermelha', value: 'Praia Vermelha' },
	{ label: 'IFCS', value: 'IFCS' },	
	{ label: 'Letras', value: "Letras" },
	{ label: 'EEFD', value: 'EEFD' },
	{ label: 'Centro de Ciências e Saúde', value: 'Centro de Ciências e Saúde' },
	{ label: 'Parque Tecnológico', value: 'Parque Tecnológico' }
];