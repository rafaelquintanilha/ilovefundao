export const projects = [
	{ 
		name: "Caronaê", 
		img: "car.jpg", 
		place: "Ilha do Fundão",
		projectTypes: ['Tecnologia', 'Jurídico', 'Vontade!'] 
	},
	{ 
		name: "Google Drive Unificado", 
		img: "drive.jpg", 
		place: "Centro de Tecnologia",
		projectTypes: ['Marketing', 'Jurídico', 'Tecnologia']  
	},
	{ 
		name: "Wi-Fi Unificado", 
		img: "wifi.jpg", 
		place: "Centro de Tecnologia",
		projectTypes: ['Financeiro', 'Jurídico'] 
	},
	{ 
		name: "Melhorias Bandeijão", 
		img: "food2.jpeg", 
		place: "Faculdade de Letras",
		projectTypes: ['Tecnologia'] 
	},
	{ 
		name: "Aulas Gravadas", 
		img: "lectures.jpg", 
		place: "Centro de Tecnologia",
		projectTypes: ['Marketing', 'Jurídico', 'Tecnologia', 'Recursos Humanos'] 
	},
	{ 
		name: "Hackathon SE", 
		img: "hackathon.jpg", 
		place: "Centro de Tecnologia",
		projectTypes: ['Marketing', 'Tecnologia', 'Financeiro'] 
	}
];