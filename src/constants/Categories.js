export const categories = [
	{ label: 'Capacitação', value: "Capacitação" },
	{ label: 'Feiras', value: "Feiras" },
	{ label: 'Ações Sociais', value: 'Ações Sociais' },
	{ label: 'Cultura', value: 'Cultura' },	
	{ label: 'Acadêmico', value: "Acadêmico" },
	{ label: 'Atividade Semanal', value: 'Atividade Semanal' },
	{ label: 'Esporte', value: 'Esporte' }
];