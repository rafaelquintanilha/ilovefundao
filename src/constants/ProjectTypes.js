export const projectTypes = [
	{ label: 'Tecnologia', value: "Tecnologia" },
	{ label: 'Financeiro', value: "Financeiro" },
	{ label: 'Recursos Humanos', value: 'Recursos Humanos' },
	{ label: 'Marketing', value: 'Marketing' },	
	{ label: 'Gestão de Projetos', value: "Gestão de Projetos" },
	{ label: 'Jurídico', value: 'Jurídico' },
	{ label: 'Vontade!', value: 'Vontade!' }
];