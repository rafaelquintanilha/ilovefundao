import React from 'react';
import { Route, IndexRoute } from 'react-router';

// Components
import Main from 'components/Main';
import ProjectsContainer from 'components/ProjectsContainer';
import AgendaContainer from 'components/AgendaContainer';
import ProductsContainer from 'components/ProductsContainer';

// NOTE: here we're making use of the `resolve.root` configuration
// option in webpack, which allows us to specify import paths as if
// they were from the root of the ~/src directory. This makes it
// very easy to navigate to files regardless of how deeply nested
// your current file is.
import ContainerApp from 'containers/ContainerApp';

export default (store) => (
	<Route path='/ilovefundao' component={ContainerApp}>
		<IndexRoute component={Main} />
		<Route path='projects' component={ProjectsContainer} />
		<Route path='agenda' component={AgendaContainer} />
		<Route path='products' component={ProductsContainer} />
	</Route>
);