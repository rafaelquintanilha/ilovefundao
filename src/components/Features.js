import React, { PropTypes } from "react";

export default class Features extends React.Component {
	render() {
		const { icon, title, body } = this.props;		
		return (
			<div className="panel panel-default">
				<div className="panel-heading">
					<center><h3><i className={`fa fa-${icon}`} /> {title}</h3></center>
				</div>
				<div className="panel-body">{body}</div>
			</div>							
		);
	}
}

Features.propTypes = {
	icon: PropTypes.string.isRequired,
	title: PropTypes.string.isRequired,
	body: PropTypes.string.isRequired
};