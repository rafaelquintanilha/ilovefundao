import React, { PropTypes } from "react";
import { places } from 'constants/Places';
import { categories } from 'constants/Categories';
import moment from 'moment';

export default class AddEvent extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: "",
			url: "",
			place: "Centro de Tecnologia",
			category: "Capacitação",
			startDate: moment().format("YYYY-MM-DD"),
			endDate: moment().format("YYYY-MM-DD"),
			time: ""
		};
	}

	handleClick() {
		this.props.add(this.state);
		this.props.close();
	}

	changeTitle(e) {
		this.setState({title: e.target.value});
	}

	changeURL(e) {
		this.setState({url: e.target.value});
	}

	changeStartDate(e) {
		this.setState({startDate: e.target.value});
	}

	changeEndDate(e) {
		this.setState({endDate: e.target.value});
	}

	changeTime(e) {
		this.setState({time: e.target.value});
	}

	changePlace(e) {
		this.setState({place: e.target.value});
	}

	changeCategory(e) {		
		this.setState({category: e.target.value});
	}

	render() {		
		return (
			<div style={{width: "600px", height: "380px"}}>				
				<div className="page-header">
					<h3>Cadastrar Evento</h3>
				</div>
				<div>
					<div className="row">
						<div className="col-sm-6">
							<label>Nome</label>
							<input 
								type="text" 
								className="form-control" 
								placeholder="Título"
								onChange={this.changeTitle.bind(this)} 
								value={this.state.title} />
						</div>
						<div className="col-sm-6">
							<label>Site</label>
							<input 
								type="text" 
								className="form-control" 
								placeholder="www.hackathonufrj.com.br"
								onChange={this.changeURL.bind(this)} 
								value={this.state.url} />
						</div>
					</div>					
					<div className="row" style={{paddingTop: "20px"}}>
						<div className="col-sm-4">
							<label>Ínicio</label>
							<input 
								type="date" 
								className="form-control"
								onChange={this.changeStartDate.bind(this)} 
								value={this.state.startDate} />
						</div>
						<div className="col-sm-4">
							<label>Término</label>
							<input 
								type="date" 
								className="form-control"
								onChange={this.changeEndDate.bind(this)} 
								value={this.state.endDate} />
						</div>
						<div className="col-sm-4">
							<label>Horário</label>
							<input 
								type="text" 
								className="form-control" 
								placeholder="Ex: 12h às 13h"
								onChange={this.changeTime.bind(this)} 
								value={this.state.time} />
						</div>
					</div>
					<div className="row" style={{paddingTop: "20px"}}>
						<div className="col-sm-6">
							<label>Lugar</label>
							<select 
								className="form-control" 
								value={this.state.place}
								onChange={this.changePlace.bind(this)}>
								{places.map((p, i) => 
									<option key={i} value={p.value}>{p.label}</option>
								)}								
							</select>
						</div>
						<div className="col-sm-6">
							<label>Categoria</label>
							<select 
								className="form-control"
								value={this.state.category}
								onChange={this.changeCategory.bind(this)}>
								{categories.map((p, i) => 
									<option key={i} value={p.value}>{p.label}</option>
								)}								
							</select>
						</div>
					</div>
				</div>
				<hr />
				<div>
					<button 
						type="button" 
						className="btn btn-danger btn-block"
						onClick={this.handleClick.bind(this)}> 					
						<i className="fa fa-plus" /> Cadastrar!
					</button>
				</div>
			</div>							
		);
	}
}

AddEvent.propTypes = {
	add: PropTypes.func.isRequired,
	close: PropTypes.func.isRequired
};