import React from "react";
import ProjectThumb from 'components/ProjectThumb';
import { projectTypes } from 'constants/ProjectTypes';
import { projects } from 'constants/Projects';
import Select from 'react-select';
import _ from 'lodash';

export default class ProjectsContainer extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			value: []
		};
	}

	handleChange(value) {		
		this.setState({value});
	}

	render() {
		const style = { marginBottom: "10px", marginTop: "30px" };				

		return (			
			<div>
				<div className="row" style={{marginTop: "30px"}}>
					<div className="col-sm-offset-2 col-sm-6">
						<div>
							<Select						
								placeholder="O que você &hearts; ?"								
								options={projectTypes}
								onChange={this.handleChange.bind(this)}
								value={this.state.value}
								simpleValue
								multi />
						</div>
					</div>
					<div className="col-sm-2">
						<button type="button" className="btn btn-default">
							<i className="fa fa-plus" /> Envie seu Projeto!
						</button>
					</div>
				</div>				
				<div className="flex-container" style={style}>
					{projects
						.filter((p, i) => {
							if (_.isEmpty(this.state.value)) return true; 
							const values = this.state.value.split(",");
							const difference = _.difference(p.projectTypes, values);
							return JSON.stringify(difference) !== JSON.stringify(p.projectTypes);
						})
						.map((p, i) => 
							<ProjectThumb project={p} key={i} />
					)}					
				</div>
			</div>
		);
	}
}