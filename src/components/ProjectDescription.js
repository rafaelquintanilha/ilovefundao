import React, { PropTypes } from "react";

export default class ProjectDescription extends React.Component {
	render() {		
		return (
			<div style={{width: "640px", height: "480px"}}>				
				<div>
					<center><img src="caronaelogo.jpg" width="55%" /></center>
				</div>
				<hr />
				<div className="row">
					<div className="col-sm-6">
						<div className="panel panel-default">
							<div className="panel-heading">
								<h3 className="panel-title">Descrição</h3>
							</div>
							<div className="panel-body">
								Caronaê é um sistema seguro e prático 
								de caronas compartilhadas, feito 
								exclusivamente para a comunidade acadêmica da Cidade 
								Universitária da UFRJ.
							</div>
						</div>
						<div className="panel panel-default">
							<div className="panel-heading">
								<h3 className="panel-title">Em que área você pode ajudar</h3>
							</div>
							<div className="panel-body">
								{this.props.project.projectTypes.map((types, i) => 
									<span 
										className="label label-danger" 
										style={{margin: "3px"}}
										key={i}>
										{types}
									</span>									
								)}
							</div>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="panel panel-default">
							<div className="panel-heading">
								<h3 className="panel-title">Responsável</h3>
							</div>
							<div className="panel-body">
								João do Silva
								<br />
								<a>joaodasilva@poli.ufrj.br</a>
							</div>
						</div>						
						<div className="panel panel-default">
							<div className="panel-heading">
								<h3 className="panel-title">
									<i className="fa fa-map-marker" />
									{' '}
									Local
								</h3>
							</div>
							<div className="panel-body">								
								{this.props.project.place}
							</div>
						</div>
					</div>					
				</div>
				<div className="row">
					<div className="col-sm-offset-3 col-sm-4">
						<button type="button" className="btn btn-default btn-lg pull-right btn-danger">
							<i className="fa fa-thumbs-up" /> Ajudar
						</button>
					</div>					
				</div>
			</div>							
		);
	}
}

ProjectDescription.propTypes = {
	project: PropTypes.object.isRequired
};