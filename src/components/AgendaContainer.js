import React from "react";

// Components
import AddEvent from 'components/AddEvent';
import Select from 'react-select';
import Modal from 'react-modal';

// Constants
import { months } from 'constants/Months';
import { places } from 'constants/Places';
import { categories } from 'constants/Categories';
import { modalStyle } from 'constants/ModalStyle';

// Util
import _ from 'lodash';
import moment from 'moment';
import update from 'react/lib/update';

export default class AgendaContainer extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			modalIsOpen: false,
			places: [],
			categories: [],
			calendar: [
				{ 
					startDate: "2016-05-17", 
					endDate: "2016-05-19", 
					time: "11h às 18h",
					title: "Feira Gastronômica",
					place: "Parque Tecnológico",
					category: "Feiras",
					url: "https://www.facebook.com/events/1006394902729634/" 
				},
				{ 
					startDate: "2016-05-16", 
					endDate: "2016-05-17", 
					time: "10h às 17h",
					title: "Feira de Estágio",
					place: "Centro de Tecnologia",
					category: "Feiras",
					url: "http://www.feiradaufrj.com.br/" 
				},
				{ 
					startDate: "2016-05-19", 
					endDate: "2016-05-19", 
					time: "13h às 14h",
					title: "Apresentação Hackathon",
					place: "Centro de Tecnologia",
					category: "Acadêmico",
					url: "http://hackathonufrj.com.br" 
				},
				{ 
					startDate: "2016-05-23", 
					endDate: "2016-05-25", 
					time: "10h às 17h",
					title: "Entrega de Kits da Cachorrada",
					place: "Centro de Tecnologia",
					category: "Esporte",
					url: "https://www.facebook.com/atleticaengenhariaufrj/" 
				},
				{ 
					startDate: "2016-05-20", 
					endDate: "2016-05-20", 
					time: "13:30h às 14:30h",
					title: "Reunião do Calet",
					place: "Letras",
					category: "Acadêmico",
					url: "https://www.facebook.com/events/895964547197321/" 
				}
			]
		};
	}

	addEvent(event) {
		const newState = update(this.state, { calendar: { $push: [event] } });
		this.setState(newState);
	}

	handleClick() {
		this.openModal();
	}

	openModal() {
		this.setState({ modalIsOpen: true });
	}

	closeModal() {
		this.setState({ modalIsOpen: false });	
	}

	handlePlacesChange(places) {		
		this.setState({places});
	}

	handleCategoriesChange(categories) {		
		this.setState({categories});
	}

	getMonth(date) {
		const array = date.split("-");
		return months[parseInt(array[1]) - 1];
	}

	getDay(date) {
		const array = date.split("-");
		return array[2];
	}

	hasDate(dates, date) {
		for ( let i = 0; i < dates.length; i++ ) {
			if ( dates[i].date === date ) return i;
		}
		return false;
	}

	mapCalendar(calendar) {
		const dates = [];

		calendar.map((e, i) => {
			const { startDate, endDate } = e;
			let currentDate = startDate;

			while ( currentDate <= endDate ) {
				const index = this.hasDate(dates, currentDate);
				if ( index === false ) {
					dates.push({ date: currentDate, events: [e] });				
				} else {
					dates[index].events.push(e);
				}
				currentDate = moment(currentDate).add(1, "day").format("YYYY-MM-DD");				
			}
		});

		return dates.sort(this.compare);
	}

	compare(a, b) {
		if ( a.date < b.date ) return -1;
		else if ( a.date > b.date ) return 1;
		return 0;
	}

	mapBackgroundColor(i) {
		return ( i % 2 === 0 ) ? 'tomato' : 'tomato-zebra';
	}

	render() {		
		const { calendar } = this.state;			

		const filtered = calendar.filter((e, i) => {
			if (_.isEmpty(this.state.places) && _.isEmpty(this.state.categories)) return true; 

			if ( !_.isEmpty(this.state.places) && _.isEmpty(this.state.categories) ) {
				const placesValue = this.state.places.split(",");
				if ( placesValue.indexOf(e.place) > -1 ) return true;
				return false;
			}

			if ( _.isEmpty(this.state.places) && !_.isEmpty(this.state.categories) ) {
				const categoriesValue = this.state.categories.split(",");
				if ( categoriesValue.indexOf(e.category) > -1 ) return true;
				return false;
			}

			const placesValue = this.state.places.split(",");
			const categoriesValue = this.state.categories.split(",");

			if ( categoriesValue.indexOf(e.category) > -1 && placesValue.indexOf(e.place) > -1 ) return true;
			return false;
		});

		const mapped = this.mapCalendar(filtered);

		return (
			<div>
				<div className="row" style={{marginTop: "30px"}}>
					<div className="col-sm-5">
						<div>
							<Select						
								placeholder="Onde você quer ir?"								
								options={places}								
								onChange={this.handlePlacesChange.bind(this)}
								value={this.state.places}
								simpleValue
								multi />
						</div>
					</div>
					<div className="col-sm-5">
						<div>
							<Select						
								placeholder="O que você quer fazer?"
								options={categories}
								onChange={this.handleCategoriesChange.bind(this)}
								value={this.state.categories}
								simpleValue
								multi />
						</div>
					</div>
					<div className="col-sm-2">
						<button 
							type="button" 
							className="btn btn-default" 
							onClick={this.handleClick.bind(this)}>
							<i className="fa fa-plus" /> Cadastre seu Evento!
						</button>
						<Modal 
							isOpen={this.state.modalIsOpen} 
							onRequestClose={this.closeModal.bind(this)} 
							style={modalStyle} >
							<AddEvent 
								add={this.addEvent.bind(this)} 
								close={this.closeModal.bind(this)} />
						</Modal>
					</div>
				</div>
				<hr />				
				{mapped.map((m, i) => 
					<div className="row" style={{paddingTop: "20px"}} key={i}>
						<div className="col-sm-offset-1 col-sm-2">
							<span style={{fontSize: "3em"}}>{this.getDay(m.date)}</span>
							<br />
							<span style={{paddingLeft: "3px"}}>{this.getMonth(m.date)}</span>
						</div>
						<div 
							className={`col-sm-8 alert alert-${this.mapBackgroundColor(i)}`} 
							style={{marginTop: "15px"}}>
							{m.events.map((e, i) => (
								<p key={i}>
									<b><a className="extern" href={e.url} target="_blank">{e.title}</a></b> 
									{' '}
									-
									{' '} 
									<i><i className="fa fa-map-marker" /> {e.place}
									{e.time !== '' ? ` - ${e.time}` : ""}
									</i>
								</p>
							))}
						</div>						
					</div>
				)}				
			</div>
		);
	}
}