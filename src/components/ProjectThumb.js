import React, { PropTypes } from "react";
import Modal from 'react-modal';
import { modalStyle } from 'constants/ModalStyle';
import ProjectDescription from 'components/ProjectDescription';

export default class ProjectThumb extends React.Component {

	constructor(props) {		
		super(props);
		this.state = { modalIsOpen: false };
	}

	handleClick() {
		this.openModal();
	}

	openModal() {
		this.setState({ modalIsOpen: true });
	}

	closeModal() {
		this.setState({ modalIsOpen: false });	
	}

	render() {
		const { name, img, place } = this.props.project;
		const style = { cursor: "pointer" };		

		return (
			<div>
				<div className="flex-item" style={style} onClick={this.handleClick.bind(this)}>						
					<div className="main" style={{backgroundImage: `url(${img})`}}>
						{name}
					</div>
					<div className="location">
						<i className="fa fa-map-marker" />
						{' '}
						{place}
					</div>						
				</div>
				<Modal 
					isOpen={this.state.modalIsOpen} 
					onRequestClose={this.closeModal.bind(this)} 
					style={modalStyle} >
					<ProjectDescription project={this.props.project} />
				</Modal>
			</div>
		);
	}
}

ProjectThumb.propTypes = {
	project: PropTypes.object.isRequired	
};