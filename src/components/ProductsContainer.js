import React from "react";
import Product from 'components/Product';

export default class ProductsContainer extends React.Component {
	render() {		
		return (
			<div style={{paddingTop: "30px"}}>
				<div className="row">
					<div className="col-sm-4">
						<Product src="Camisa 01.png" />
					</div>
					<div className="col-sm-4">
						<Product src="Camisa 02.png" />
					</div>
					<div className="col-sm-4">
						<Product src="Camisa 03.png" />
					</div>
				</div>
				<div className="row" style={{paddingTop: "20px"}}>
					<div className="col-sm-4">
						<Product src="Camisa 04.png" />
					</div>
					<div className="col-sm-4">
						<Product src="Camisa 05.png" />
					</div>
					<div className="col-sm-4">
						<Product src="Camisa 06.png" />
					</div>
				</div>
				<div className="row" style={{paddingTop: "20px"}}>
					<div className="col-sm-4">
						<Product src="Capa 01.png" alt="Capa 02.png" />
					</div>
					<div className="col-sm-4">
						<Product src="Capa 03.png" alt="Capa 04.png" />
					</div>
					<div className="col-sm-4">
						<Product src="Capa 05.png" />
					</div>
				</div>
				<div className="row" style={{paddingTop: "20px"}}>
					<div className="col-sm-4">
						<Product src="Caneca 1.1.png" alt="Caneca 1.2.png" />
					</div>
					<div className="col-sm-4">
						<Product src="Caneca 2.1.png" alt="Caneca 2.2.png" />
					</div>
					<div className="col-sm-4">
						<Product src="Caneca 3.1.png" alt="Caneca 3.2.png" />
					</div>
				</div>
				<div className="row" style={{paddingTop: "20px"}}>
					<div className="col-sm-4">
						<Product src="Caneca 4.1.png" alt="Caneca 4.2.png" />
					</div>
					<div className="col-sm-4">
						<Product src="Caneca 5.1.png" alt="Caneca 5.2.png" />
					</div>					
				</div>
			</div>							
		);
	}
}