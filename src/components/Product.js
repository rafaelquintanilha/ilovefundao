import React, { PropTypes } from "react";
import ZoomableImage from 'components/ZoomableImage';

export default class Product extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			count: 0
		};
	}

	handleEnter() {
		this.setState({count: this.state.count + 1});		
	}

	handleLeave() {
		this.setState({count: this.state.count + 1});
	}

	render() {
		let src;

		if ( this.props.alt ) {
			src = this.state.count % 2 === 0 ? this.props.src : this.props.alt;						
		} else {
			src = this.props.src;	
		}

		src = '/ilovefundao/' + src;

		return (
			<div>
				<center>
					<div onMouseEnter={this.handleEnter.bind(this)} onMouseLeave={this.handleLeave.bind(this)}>
						<ZoomableImage src={src}>
							<img src={src} width="200px" height="200px" />
						</ZoomableImage>
					</div>
					<p style={{paddingTop: "20px"}}>
						<button 
							type="button" 
							className="btn btn-danger">
							<i className="fa fa-heart" /> Quero!
						</button>
					</p>						
				</center>
			</div>							
		);
	}
}

Product.propTypes = {
	src: PropTypes.string.isRequired,
	alt: PropTypes.string
};