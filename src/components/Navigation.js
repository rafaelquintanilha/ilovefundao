import React, { PropTypes } from "react";
import { Link } from 'react-router';

export default class Navigation extends React.Component {

	constructor(props) {
		super(props);		
		this.URL = {
			home: "/ilovefundao",
			agenda: "/ilovefundao/agenda",
			projects: "/ilovefundao/projects",			
			products: "/ilovefundao/products",
			contact: "/ilovefundao/contact"
		};
	}

	setClass(URL) {
		return this.props.URL === URL ? 'active' : '';
	}	

	render() {		
		const { home, projects, agenda, products, contact } = this.URL;

		return (
			<div>
				<ul className="navigation logo">
					<li className={this.setClass(home)}>
						<Link to={home}>							
							<b>I &hearts; Fundão</b>
						</Link>
					</li>
					<li className={this.setClass(agenda)}>
						<Link to={agenda}>
							&hearts; Agenda
						</Link>
					</li>
					<li className={this.setClass(projects)}>
						<Link to={projects}>
							&hearts; Projetos
						</Link>
					</li>									
					<li className={this.setClass(products)}>
						<Link to={products}>&hearts; Produtos</Link>
					</li>
					<li className={this.setClass(contact)}>
						<Link to="/#">Contato</Link>
					</li>
				</ul>
			</div>		
		);
	}
}

Navigation.propTypes = {
	URL: PropTypes.string.isRequired
};