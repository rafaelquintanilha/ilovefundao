import React, { PropTypes } from 'react';
import Modal from 'react-modal';

export default class ZoomableImage extends React.Component {

	constructor(props) {		
		super(props);		
		this.state = { modalIsOpen: false };
	}

	openModal() {
		this.setState({ modalIsOpen: true });
	}

	closeModal() {
		this.setState({ modalIsOpen: false });	
	}	

	render() {
		const modalStyle = {
			content: {
				top: '50%',
				left: '50%',
				right: 'auto',
				bottom: 'auto',
				marginRight: '-50%',
				transform: 'translate(-50%, -50%)'
			}
		};
		return (
			<div>
				<div className="hasImage" onClick={this.openModal.bind(this)}>
					{this.props.children}
				</div>
				<Modal 
					isOpen={this.state.modalIsOpen} 
					onRequestClose={this.closeModal.bind(this)} 
					style={modalStyle} >
					<img src={this.props.src} width="640px" height="auto" />
				</Modal>				
			</div>
		);
	}
}

ZoomableImage.propTypes = {	
	src: PropTypes.string,
	children: PropTypes.object
};