import React from "react";

// Components
import Header from 'components/Header';
import Features from 'components/Features';

// CSS
import '../styles/core.scss';

export default class Main extends React.Component {
	render() {		
		const productsBody = `Mostre pra todo mundo o seu orgulho pela UFRJ!
			Adquira um de nossos produtos e nos ajude a tirar do papel 
			projetos de melhorias pra UFRJ!`;

		const projectsBody = `Conheça os projetos de melhoria para UFRJ que nós apoiamos!
			Gostou de algum? Quer nos ajudar a fazer uma UFRJ melhor?
			Só escolher o que mais gostou e mão na massa!`;

		const agendaBody = `Está de bobeira entre as aulas da faculdade?
			Da uma passada na nossa Agenda e cheque tudo de maneiro que está rolando na UFRJ.
			Está organizando um evento bacana e quer divulgar pra galera?
			Conte para gente que te ajudamos a divulgar.`;

		return (
			<div>								
				<Header />
				<div className="row">
					<div className="col-sm-3">
						<h3 style={{textAlign: "center"}}>O Movimento</h3>
						<hr />
					</div>
					<div className="col-sm-8">
						<p>Inspirados pelo movimento <a href="iloveny.com">I Love NY</a>, esse é um movimento promovido pelos 
						alunos que tem como objetivo central:</p>
						<p><b>
							“Estimular sentimento de empatia, orgulho e pertencimento à UFRJ”.
						</b></p>
						<p>Acreditamos que:</p>
						<p>
							“Atingindo nosso objetivo, estimulamos um comportamento mais proativo, 
							ético e responsável no Fundão/ UFRJ, desta forma contribuindo para o Fundão 
							seja um lugar melhor”.
						</p>
					</div>
				</div>								
				<div className="row" style={{marginTop: "30px"}}>
					<div className="col-sm-4">
						<Features icon="book" title="Agenda" body={agendaBody} />						
					</div>
					<div className="col-sm-4">
						<Features icon="area-chart" title="Projetos" body={projectsBody} />						
					</div>
					<div className="col-sm-4">
						<Features icon="shopping-cart" title="Produtos" body={productsBody} />						
					</div>
				</div>				
			</div>			
		);
	}
}