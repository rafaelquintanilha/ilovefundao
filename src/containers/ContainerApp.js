import React, { PropTypes } from 'react';
import Navigation from 'components/Navigation';

export default class ContainerApp extends React.Component {
	render () {
		const URL = this.props.location.pathname;
		return (
			// Put all app's global styles here			
			<div>
				<Navigation URL={URL} />
				<div className="container">
					{this.props.children}
				</div>
			</div>
		);
	}
}

ContainerApp.propTypes = {
	children: PropTypes.object,
	location: PropTypes.object
};