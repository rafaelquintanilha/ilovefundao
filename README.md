I Love Fundão
===============

Repositório do sistema desenvolvido durante a 1ª Hackathon UFRJ.

Originally cloned from [React-Redux-Starter-Kit](https://github.com/davezuko/react-redux-starter-kit)

Run in development mode:

```shell
$ npm run dev:no-debug
```

Compile into production

```shell
$ npm run deploy
```

Refer to the original repo for all available scripts.

Use TAB as line identation!